/**
 * 888                             888
 * 888                             888
 * 88888b.   8888b.  88888b.d88b.  88888b.   .d88b.  888d888  8888b.
 * 888 "88b     "88b 888 "888 "88b 888 "88b d88""88b 888P"       "88b
 * 888  888 .d888888 888  888  888 888  888 888  888 888     .d888888
 * 888 d88P 888  888 888  888  888 888 d88P Y88..88P 888     888  888
 * 88888P"  "Y888888 888  888  888 88888P"   "Y88P"  888     "Y888888
 *
 * @category    Samples
 * @package     apac-sdk-java
 * @author      Bambora Online APAC
 * @copyright   Bambora (http://bambora.com)
 */
package samples.examples;

import com.bambora.Client;
import com.bambora.SessionInitiationRequestObject;

import java.math.BigInteger;
import java.security.SecureRandom;

import static com.bambora.EndPoint.DEMO_URL;

public class SessionTokenInitationExample {

    public static final String USERNAME = "lukechiam.api";
    public static final String PASSWORD = "qwer1234";
    public static final String DL = "standard_purchase";
    private static SecureRandom random = new SecureRandom();

    public static void main(String[] args) {
        Client client = new Client(USERNAME, PASSWORD, DL, DEMO_URL);

        /**
         * get session token for each new shopping cart session
         */
        String sessionToken = client.getSessionToken(new SessionInitiationRequestObject(randomInt(), randomString(),
                randomString(), "http://demo.bambora.com", "http://demo.bambora.com", randomString()));


    }

    private static String randomString() {
        return new BigInteger(130, random).toString(32);
    }

    private static Integer randomInt() {
        return new BigInteger(8, random).intValue();
    }

}
